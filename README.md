# Projet TinyTwitter

## Auteurs
* Nicolas VANNIER
* Tom LE BERRE
* Maxence COUTAND
* Axel RAMBAUD

## Mesure Tweet

Post d'un tweet avec 100 followers
```
(135+152+145+130+123+116+119+109+120+109+108+100+112+14+109+104+109+109+110+102+112+94+104+89+11+110+96+95+107+105)/30 = 105.26 ms
Variance: 805
```
Post d'un tweet avec 1000 followers
```
(307+733+649+802+905+670+728+709+733+671+898+618+677+656+616+616+657+656+645+685+648+616+704+609+665+682+713+669+706+712)/30 = 678.5 ms
Variance: 9695
```
Post d'un tweet avec 5000 followers
```
(1097+1458+865+2913+960+2824+3000+2971+2967+3005+3029+3029+2992+3014+3060+3111+3300+3132+3049+1810+1490+1420+1458+1526+1443+1617+1659+1474+1481+1716)/30 = 2229 ms
Variance: 673897
```

## Mesure HashTag

Mesurer combien de temp en moyenne (sur 30 mesures), pour extraire les 50 derniers message pour une hashtag concernant 1000 et 5000 messages. 


Test Hashtag sur 1000 messages
```
(38+28+28+26+21+21+19+19+21+18+20+28+16+17+17+19+17+15+18+19+16+17+14+14+15+14+16+14+14+15)/30 = 19.13 ms
Variance: 29.45
```
Test Hashtag sur 5000 messages
```
(33+24+25+24+24+20+21+18+0+30+18+9+19+319+20+19+19+21+17+22+20+19+19+19+16+16+17+15+17+17)/30 = 29.2
Variance: 2927.31
```

## Mesure timeLine

Pour chaque configuration de follower (100,1000, 5000), combien de temps en moyenne (sur 30 mesures) pour extraire ses 10 derniers messages, 50 derniers messages et 100 derniers messages 


#### Pour 100 followers
Test charger 10 dernier message de la TimeLine d'un user qui suit 100 autres users
```
(14+11+10+9+12+10+12+13+10+14+8+10+11+13+8+10+15+13+14+7+12+15+13+12+11+12+10+11+9+12)/30 = 11.36 ms
Variance: 4.17
```

Test charger 50 dernier message de la TimeLine d'un user qui suit 100 autres users
```
(16+16+15+16+12+22+16+16+21+17+16+17+17+21+18+16+18+17+24+16+15+18+17+16+18+18+14+15+16+15)/30 = 16.96 ms
Variance: 5.70
```
Test charger 100 dernier message de la TimeLine d'un user qui suit 100 autres users
```
(21+19+21+17+21+18+28+19+29+19+18+20+20+27+21+19+21+18+27+17+17+17+18+17+21+28+18+26+20+19)/30 = 20.7 ms
Variance: 13.48
```
#### Pour 1000 followers

Test charger 10 dernier message de la TimeLine d'un user qui suit 1000 autres users
```
(26+12+15+17+12+15+12+16+15+16+13+14+18+13+15+14+16+18+17+11+13+16+12+114+18+13+15)/30 = 16.86 ms
Variance: 357.53
```
Test charger 50 dernier message de la TimeLine d'un user qui suit 1000 autres users
```
(18+26+22+21+24+18+16+21+18+22+18+20+16+22+20+17+19+14+16+17+10+12+17+18+14+20+15+22+19+21)/30 = 18.43 ms
Variance: 11.85
```
Test charger 100 dernier message de la TimeLine d'un user qui suit 1000 autres users
```
(21+20+29+23+20+21+20+20+24+20+18+20+21+19+22+19+27+23+17+18+20+25+21+22+19+18+24+20+21+20)/30 = 21.06 ms
Variance: 6.93
```
#### Pour 5000 followers

Test charger 10 dernier message de la TimeLine d'un user qui suit 5000 autres users
```
(37+26+19+17+16+16+25+24+23+15+16+18+19+17+21+16+16+25+17+19+17+19+22+17+21+20+19+18+20+19)/30 = 19.80 ms
Variance: 18.96
```
Test charger 50 dernier message de la TimeLine d'un user qui suit 5000 autres users
```
(28+28+26+22+26+23+25+27+33+24+23+23+29+26+27+21+24+28+26+23+25+27+20+26+19+28+24+28+23)/30 = 24.4 ms
Variance: 8.60
```
Test charger 100 dernier message de la TimeLine d'un user qui suit 5000 autres users
```
(26+27+24+28+30+28+25+25+26+24+33+28+27+25+26+28+33+26+25+24+24+38+20+21+21+27+21+24+26+29)/30 = 26.3 ms
Variance:13.94
```
## Liens utiles

* [TinyTwitter coté serveur](https://gitlab.com/max_c/tinytwin)
* [TinyTwitter coté client](https://gitlab.com/vnnc/angular-tinytwitt)
* [TinyTwitter google doc](https://docs.google.com/document/d/1QjA0ck_fDrDrpGr4KvRKepUZawimsuv35Vygb22bfnU/edit?fbclid=IwAR3DiwFBl9FzU6fUr56JoQaDGRZ5YJoBahtPCLvhXRcTcGeVI1uAJJwOV0E)