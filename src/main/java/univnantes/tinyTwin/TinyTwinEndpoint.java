package univnantes.tinyTwin;



import java.util.List;
import java.util.Map;

import org.apache.http.HttpStatus;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;

import univnantes.tinyTwin.DAO.ServiceDao;
import univnantes.tinyTwin.DAO.model.MessageEntity;
import univnantes.tinyTwin.DAO.model.UserEntity;


@Api(name = "myApi")
public class TinyTwinEndpoint {
	

	ServiceDao dao = new ServiceDao();


	@ApiMethod(
	        path = "services/listeM",
	        httpMethod = HttpMethod.GET)
	public List<MessageEntity> getM() {
			return dao.getMessage();
	}
	
	@ApiMethod(
	        path = "services/postMessage",
	        httpMethod = HttpMethod.POST)
	public MessageEntity postMessage(@Named("idUser") String idUser, @Named("message") String text,  @Named("hashTag") String hashTag) {
		return dao.postMessage(idUser, text, hashTag);
	}

	@ApiMethod(
	        path = "services/createUser",
	        httpMethod = HttpMethod.POST)
	public List<UserEntity> createUser(@Named("name") String name) {
		dao.createUser(name);
		return dao.getUsers();
	}
	
	@ApiMethod(
	        path = "services/getTimeLine",
	        httpMethod = HttpMethod.POST)
	public List<MessageEntity> getTimeLine(@Named("idUser") String idUser) {
		return dao.getTimeLine(idUser);
	}
	
	@ApiMethod(
	        path = "services/getTimeLineWithLimit",
	        httpMethod = HttpMethod.POST)
	public List<MessageEntity> getTimeLineWithLimit(@Named("idUser") String idUser, @Named("limit") String limit) {
		return dao.getTimeLineWithLimit(idUser, Integer.valueOf(limit));
	}
	
	
	@ApiMethod(
	        path = "services/followUser",
	        httpMethod = HttpMethod.POST)
	public Object followUser(@Named("idUser") String idUser, @Named("newIdUserFollow") String newIdUserFollow) {
		dao.followUser(idUser, newIdUserFollow);
		return HttpStatus.SC_ACCEPTED;
	}
	
	@ApiMethod(
	        path = "services/createUserForScalableTests",
	        httpMethod = HttpMethod.POST)
	public Map<String, String> createUserForScalableTests() {
		return dao.createUserForScalableTests();
	}
	
	@ApiMethod(
	        path = "services/clean",
	        httpMethod = HttpMethod.POST)
	public Map<String, Long> clean() {
		return dao.clean();
	}
	
	@ApiMethod(
	        path = "services/getHashtag",
	        httpMethod = HttpMethod.POST)
	public List<MessageEntity> getHashtag(@Named("hashTag") String hashTag) {
		return dao.getHashtag(hashTag);
	}
	
	@ApiMethod(
	        path = "services/getHashtagWithLimit",
	        httpMethod = HttpMethod.POST)
	public List<MessageEntity> getHashtagWithLimit(@Named("hashTag") String hashTag, @Named("limit") String limit) {
		return dao.getHashtagWithLimit(hashTag, Integer.valueOf(limit));
	}
}