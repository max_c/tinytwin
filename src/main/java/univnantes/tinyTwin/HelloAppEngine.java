package univnantes.tinyTwin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import univnantes.tinyTwin.DAO.ServiceDao;

@SuppressWarnings("serial")
@WebServlet(name = "HelloAppEngine", urlPatterns = { "/hello" })
public class HelloAppEngine extends HttpServlet {

	ServiceDao dao = new ServiceDao();

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");

			response.getWriter().print("hello");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}