package univnantes.tinyTwin.DAO.model;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Index
public class TimeLineEntity {

	@Id
	private String idUser;
	
	private List<Long> messages;

	public TimeLineEntity() {
		super();
		messages = new ArrayList<Long>();
	}

	public TimeLineEntity(String idUser) {
		super();
		messages = new ArrayList<Long>();
		this.idUser = idUser;
	}
	
	public TimeLineEntity(String idUser, List<Long> messages) {
		super();
		this.idUser = idUser;
		this.messages = messages;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public List<Long> getMessages() {
		return messages;
	}

	public void setMessages(List<Long> messages) {
		this.messages = messages;
	}
	
	
}
