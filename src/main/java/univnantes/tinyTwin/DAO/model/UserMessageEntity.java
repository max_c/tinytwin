package univnantes.tinyTwin.DAO.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Index
public class UserMessageEntity {
	@Id
	private Long id;
	
	private String idUser;
	
	private Long idMessage;

	public UserMessageEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserMessageEntity(String idUser, Long idMessage) {
		super();
		this.idUser = idUser;
		this.idMessage = idMessage;
	}
	
	public Long getId() {
		return this.id;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public Long getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(Long idMessage) {
		this.idMessage = idMessage;
	}
	
	

}
