package univnantes.tinyTwin.DAO.model;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;

@Entity
@Index
public class MessageEntity {

	@Id
	private Long idMessage;

	@Unindex
	private String content;

	private String hashTag;

	private Date date;
	
	private String autor;

	public MessageEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public MessageEntity(String autor, String content) {
		super();
		this.autor = autor;
		this.content = content;
		this.date = new Date();
	}
	
	public MessageEntity(String autor, String content, String hashTag) {
		super();
		this.autor = autor;
		this.content = content;
		this.hashTag = hashTag;
		this.date = new Date();
	}

	public Long getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(Long idMessage) {
		this.idMessage = idMessage;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getHashTag() {
		return hashTag;
	}

	public void setHashTag(String hashTag) {
		this.hashTag = hashTag;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}
	
	
}
