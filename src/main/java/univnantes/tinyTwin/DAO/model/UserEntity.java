package univnantes.tinyTwin.DAO.model;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Index
public class UserEntity {

	@Id
	private String nom;

	private List<String> followers;

	private List<String> follow;

	public UserEntity() {
		followers = new ArrayList<String>();
		follow = new ArrayList<String>();
	}

	public UserEntity(String nom) {
		followers = new ArrayList<String>();
		follow = new ArrayList<String>();
		this.nom = nom;
	}

	public UserEntity(String nom, List<String> followers, List<String> follow) {
		super();
		this.nom = nom;
		this.followers = followers;
		this.follow = follow;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<String> getFollowers() {
		return followers;
	}

	public void setFollowers(List<String> followers) {
		this.followers = followers;
	}

	public List<String> getFollow() {
		return follow;
	}

	public void setFollow(List<String> follow) {
		this.follow = follow;
	}

}
