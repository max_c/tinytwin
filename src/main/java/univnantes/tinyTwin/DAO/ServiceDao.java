package univnantes.tinyTwin.DAO;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.VoidWork;

import univnantes.tinyTwin.DAO.model.MessageEntity;
import univnantes.tinyTwin.DAO.model.TimeLineEntity;
import univnantes.tinyTwin.DAO.model.UserEntity;
import univnantes.tinyTwin.DAO.model.UserMessageEntity;

public class ServiceDao {

	static {
		ObjectifyService.register(MessageEntity.class);
		ObjectifyService.register(UserEntity.class);
		ObjectifyService.register(TimeLineEntity.class);
		ObjectifyService.register(UserMessageEntity.class);
		ObjectifyService.register(UserEntity.class);

	}

	

	public void createUser(String name) {

		UserEntity user = new UserEntity(name);
		ofy().save().entity(user).now();
	}

	public List<UserEntity> getUsers(String name) {
		return ofy().load().type(UserEntity.class).limit(10).list();
	}

	/**
	 * creer le message, l'assigne à l'utilisateur, puis l'ajoute dans la timeLine de tout les followers
	 * de la personne qui post le message
	 * 
	 * @param idUser
	 * @param message
	 * @param hashTag
	 */
	public MessageEntity postMessage(String idUser, String message, String hashTag) {
		MessageEntity msg = new MessageEntity(idUser ,message, hashTag);
		ofy().save().entity(msg).now();

		UserMessageEntity ume = new UserMessageEntity(idUser, msg.getIdMessage());
		ofy().save().entity(ume).now();

		ofy().load().type(UserEntity.class).id(idUser).now().getFollowers().iterator().forEachRemaining(idFollower -> {
			ofy().transact(new VoidWork() {

				TimeLineEntity timeLine;

				@Override
				public void vrun() {
					timeLine = ofy().load().type(TimeLineEntity.class).id(idFollower).now();
					if(timeLine == null) {
						timeLine = new TimeLineEntity(idFollower);
					}
					timeLine.getMessages().add(msg.getIdMessage());
					ofy().save().entity(timeLine).now();
				}

			});
		});
		
		return msg;
	}

	/**
	 * retourne la timeLine de la personne passer en parametre
	 * @param idUser
	 * @return
	 */
	public List<MessageEntity> getTimeLine(String idUser) {
		TimeLineEntity timeLine = ofy().load().type(TimeLineEntity.class).id(idUser).now();
		if(timeLine == null) return null;
		
		List<MessageEntity> listMessages = new ArrayList<MessageEntity>();
		timeLine.getMessages().forEach(idMessage -> listMessages.add(ofy().load().type(MessageEntity.class).id(idMessage).now()));
		return listMessages;
	}
	
	/**
	 * retourne la timeLine avec un nombre max de message de la personne passer en parametre
	 * @param idUser
	 * @return
	 */
	public List<MessageEntity> getTimeLineWithLimit(String idUser, int limit) {
		TimeLineEntity timeLine = ofy().load().type(TimeLineEntity.class).id(idUser).now();
		if(timeLine == null || limit > timeLine.getMessages().size()) return null;
		
		List<MessageEntity> listMessages = new ArrayList<MessageEntity>();
		int i=0;
		while(i<limit) {
			listMessages.add(ofy().load().type(MessageEntity.class).id(timeLine.getMessages().get(i)).now());
			i++;
		}
		return listMessages;
	}
	
	/**
	 * permet a idUser de suivre newIdUserFollow
	 * @param idUser
	 * @param newIdUserFollow
	 */
	public void followUser(String idUser, String newIdUserFollow) {
		//ajout de newIdUserFollow dans la liste des users que suit idUser 
		UserEntity user = ofy().load().type(UserEntity.class).id(idUser).now();
		user.getFollow().add(newIdUserFollow);
		ofy().save().entity(user).now();
			
		//ajout de idUser dans la liste followers de newIdUserFollow  (ceux qui le suive)
		UserEntity userFollow = ofy().load().type(UserEntity.class).id(newIdUserFollow).now();
		userFollow.getFollowers().add(idUser);
		ofy().save().entity(userFollow).now();
			
	}
	
	
	public List<MessageEntity> getMessage() {
		return ofy().load().type(MessageEntity.class).order("date").limit(5).list();		
	}

	public List<UserEntity> getUsers() {
		return ofy().load().type(UserEntity.class).limit(5).list();		
	}

	/**
	 * creer les 5000 users necessaires si il n'existe pas deja et on creer les 
	 * 3 users suivant chaqu'un 100, 1000 et 5000 users
	 * @return
	 */
	public Map<String, String> createUserForScalableTests() {
		//creers les 3 users pour les tests
		UserEntity userFollow100 = new UserEntity("userFollow100");
		UserEntity userFollow1000 = new UserEntity("userFollow1000");
		UserEntity userFollow5000 = new UserEntity("userFollow5000");

		ofy().save().entity(userFollow100).now();
		ofy().save().entity(userFollow1000).now();
		ofy().save().entity(userFollow5000).now();
				
		//creation des users restant
		UserEntity u;	
		for(int i = 0;i<5000;++i) {
			u = new UserEntity("user"+i);
			ofy().save().entity(u).now();
			if(i<100){
				this.followUser(userFollow100.getNom(), u.getNom());//userFollow100 suit  l.get(i).getNom()
				this.followUser(u.getNom(), userFollow100.getNom());//l.get(i).getNom() suit userFollow100
			}
			if(i<1000) {
				this.followUser(userFollow1000.getNom(), u.getNom());
				this.followUser(u.getNom(), userFollow1000.getNom());
			}
			this.followUser(userFollow5000.getNom(), u.getNom());
			this.followUser(u.getNom(), userFollow5000.getNom());
		}
		
		//post des messages pour timeline
		String nomUser;
		for(int i=0;i<100;++i) {
			nomUser = "user"+i;
			this.postMessage(nomUser, "message"+i, "hashTag");
		}
		
		//post des messages pour recherche hashtag
		for(int i=0;i<1000;++i) {
			this.postMessage("user2500", "message", "hashTag1000");
		}
		for(int i=0;i<5000;i++){
			this.postMessage("user4900", "message", "hashTag5000");
		}
		
		//retour des 3 users
		Map<String, String> result = new HashMap<String, String>();
		result.put("userFollow100", ofy().load().type(UserEntity.class).id(userFollow100.getNom()).now().getNom());
		result.put("userFollow1000", ofy().load().type(UserEntity.class).id(userFollow1000.getNom()).now().getNom());
		result.put("userFollow5000", ofy().load().type(UserEntity.class).id(userFollow5000.getNom()).now().getNom());
		
		return result;
	
	}

	public Map<String, Long> clean() {

		ofy().load().type(UserEntity.class).list().iterator().forEachRemaining(user -> ofy().delete().type(UserEntity.class).id(user.getNom()).now());
		ofy().load().type(MessageEntity.class).list().iterator().forEachRemaining(user -> ofy().delete().type(MessageEntity.class).id(user.getIdMessage()).now());
		ofy().load().type(UserMessageEntity.class).list().iterator().forEachRemaining(id -> ofy().delete().type(UserMessageEntity.class).id(id.getId()).now());

		return null;
	}
	

	public List<MessageEntity> getHashtag(String hashTag) {
		return ofy().load().type(MessageEntity.class).filter("hashTag", hashTag ).order("-date").list();
	}

	public List<MessageEntity> getHashtagWithLimit(String hashTag, int limit) {
		return ofy().load().type(MessageEntity.class).filter("hashTag", hashTag ).order("-date").limit(limit).list();
	}

}